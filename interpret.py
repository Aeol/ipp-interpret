'''
IPP BUT - IPPcode19 interpret
author: Samuel Mensak
date: 11.4.2019
'''

import sys
import re
import argparse
import xml.etree.ElementTree as ET
from xml.sax.saxutils import unescape

INSTRUCTIONS = ["MOVE", "CREATEFRAME", "PUSHFRAME", "POPFRAME", "DEFVAR", "CALL", "RETURN", "PUSHS", "POPS", "ADD",
				"SUB", "MUL", "IDIV", "LT", "GT", "EQ", "AND", "OR", "NOT", "INT2CHAR", "STRI2INT", "READ", "WRITE",
				"CONCAT", "EXIT", "STRLEN", "GETCHAR", "SETCHAR", "TYPE", "LABEL", "JUMP", "JUMPIFEQ", "JUMPIFNEQ",
				"DPRINT", "BREAK"]

FRAMES = ["GF", "TF", "LF"]
TYPES = ["bool", "int", "string", "nil"]

TYPE_NONE = 0
TYPE_INT = 1
TYPE_BOOL = 2
TYPE_STRING = 3
TYPE_NIL = 4


def errorMessage(code, message): # exit with message and code
	sys.stderr.write(message + "\n")
	exit(code)


class parseError(Exception): # for error handling
	pass


class Frames: # frames class, methods should be eazy to understand
	def __init__(self):

		self.frame_stack = []
		self.global_frame = []
		self.local_frame = None
		self.get_vars_stats = None
		self.temporary_frame = None
		self.stat_vars = 0
		self.vars_current = 0

	def push_f(self):

		if self.temporary_frame is not None:
			self.frame_stack.append(self.temporary_frame)
			self.local_frame = self.temporary_frame
			self.temporary_frame = None
		else:
			errorMessage(55, "TF not defined!")

	def pop_f(self):

		if len(self.frame_stack) != 0:
			self.temporary_frame = self.frame_stack.pop()
			try:
				self.local_frame = self.frame_stack[-1]
			except IndexError:
				self.local_frame = None
		else:
			errorMessage(55, "Frame stack is empty!")

	def add_to_gf(self, variable):

		if self.get_from_gf(variable.name, False):
			sys.stderr.write("Variable %s already defined in global frame!\n" % variable.name)
			exit(52)
		self.global_frame.append(variable)

	def add_to_lf(self, variable):

		if self.local_frame is None:
			errorMessage(55, "LF is not initialized!")

		if not self.get_from_lf(variable.name, False):
			self.local_frame.append(variable)

	def add_to_tf(self, variable):

		if self.temporary_frame is None:
			errorMessage(55, "TF is not initialized!")

		if self.get_from_tf(variable.name, False):
			sys.stderr.write("Variable %s already defined in temporary frame!\n" % variable.name)
			exit(52)
		self.temporary_frame.append(variable)

	def get_from_lf(self, name, error_if_not_found=True):

		if self.local_frame is None:
			errorMessage(55, "LF is not defined!")

		for var in self.local_frame:
			if var.name == name:
				return var

		if error_if_not_found:
			sys.stderr.write("Variable %s is not defined in LF!\n" % name)
			exit(54)

		return None

	def get_from_tf(self, name, error_if_not_found=True):

		if self.temporary_frame is None:
			errorMessage(55, "TF is not defined!")

		for var in self.temporary_frame:
			if var.name == name:
				return var

		if error_if_not_found:
			sys.stderr.write("Variable %s is not defined in TF!\n" % name)
			exit(54)

		return None

	def get_from_gf(self, name, error_if_not_found=True):

		for var in self.global_frame:
			if var.name == name:
				return var

		if error_if_not_found:
			sys.stderr.write("Variable %s is not defined in GF!\n" % name)
			exit(54)

		return None


class VariablesFactory: # class for vars, 

	def __init__(self, frames):
		self.frames = frames

	def def_var(self, var):

		frame, name = var[1].split("@")
		new_variable = Variable(name)

		if frame == "GF":
			self.frames.add_to_gf(new_variable)
		elif frame == "TF":
			self.frames.add_to_tf(new_variable)
		elif frame == "LF":
			self.frames.add_to_lf(new_variable)

	def move_to_var(self, var, symb):

		variable = self.get_var(var)

		if symb[0] == "string":
			variable.variable_type = TYPE_STRING
			variable.value = symb[1]
		elif symb[0] == "nil":
			variable.variable_type = TYPE_NIL
			variable.value = symb[1]
		elif symb[0] == "int":
			variable.variable_type = TYPE_INT
			variable.value = symb[1]
		elif symb[0] == "bool":
			variable.variable_type = TYPE_BOOL
			variable.value = symb[1]
		elif symb[0] == "var":
			symb_var = self.get_var(symb, True)
			variable.value = symb_var.value
			variable.variable_type = symb_var.variable_type

	def get_var(self, var, check_if_initialized=False):

		frame, name = var[1].split("@")

		variable = None

		if frame == "GF":
			variable = self.frames.get_from_gf(name)
		elif frame == "TF":
			variable = self.frames.get_from_tf(name)
		elif frame == "LF":
			variable = self.frames.get_from_lf(name)

		if variable.variable_type == TYPE_NONE and check_if_initialized:
			sys.stderr.write("%s not initialized!\n" % var[1])
			exit(56)

		return variable

	def print_var(self, symb, debug=False):

		if symb[0] == "var":
			variable = self.get_var(symb, True)
			data_type = variable.variable_type
			value = variable.value
		else:
			data_type, value = self.get_symb_type_value(symb)

		if value is None:
			sys.stderr.write("%s does not have value!\n" % symb[1])
			exit(56)

		if debug:
			sys.stderr.write(value)
		else:
			print(value,end='')

	def aritmetic_op(self, var, symb1, symb2, operation):

		variable = self.get_var(var)
		symb1_type, symb1_value = self.get_symb_type_value(symb1)
		symb2_type, symb2_value = self.get_symb_type_value(symb2)

		if symb1_type != TYPE_INT:
			sys.stderr.write("Aritmetic instructions requires two int types!\n")
			self.exit_wrong_op(symb1, symb2, types=[TYPE_INT])
		elif operation == "idiv" and symb1_type != TYPE_INT:
			sys.stderr.write("IDIV instruction requires two int types!\n")
			self.exit_wrong_op(symb1, symb2, types=[TYPE_INT])
		elif symb1_type != symb2_type:
			errorMessage(53, "Aritmetic instruction requires same types!")

		variable.variable_type = symb1_type

		if operation == "add":
			variable.value = symb1_value + symb2_value
		elif operation == "sub":
			variable.value = symb1_value - symb2_value
		elif operation == "mul":
			variable.value = symb1_value * symb2_value
		elif operation == "idiv":
			try:
				variable.value = symb1_value // symb2_value
			except ZeroDivisionError:
				errorMessage(57, "IDIV division by zero!")

	def relation_op(self, var, symb1, symb2, operator):

		variable = self.get_var(var)
		symb1_type, symb1_value = self.get_symb_type_value(symb1)
		symb2_type, symb2_value = self.get_symb_type_value(symb2)

		if symb1_type != symb2_type:
			errorMessage(53, "Both symbols must have same type for relation operators use!")

		if symb1_type == TYPE_NIL or symb2_type == TYPE_NIL:
			if operator != "eq":
				errorMessage(53, "nil with wrong instruction!\n")

		variable.variable_type = TYPE_BOOL

		if operator == "lt":
			variable.value = str(symb1_value < symb2_value).lower()
		elif operator == "gt":
			variable.value = str(symb1_value > symb2_value).lower()
		elif operator == "eq":
			variable.value = str(symb1_value == symb2_value).lower()

	def bool_op(self, var, symb1, symb2, operator):

		variable = self.get_var(var)
		symb1_type, symb1_value = self.get_symb_type_value(symb1)
		symb2_type, symb2_value = self.get_symb_type_value(symb2)

		if symb1_type != symb2_type or symb1_type != TYPE_BOOL:
			sys.stderr.write("Bool instructions can only have two symbols with bool types!\n")
			self.exit_wrong_op(symb1, symb2, types=[TYPE_BOOL])

		if symb1_value == "true":
			symb1_actual = True
		else:
			symb1_actual = False

		if symb2_value == "true":
			symb2_actual = True
		else:
			symb2_actual = False

		variable.variable_type = TYPE_BOOL

		if operator == "and":
			variable.value = str(symb1_actual and symb2_actual).lower()
		elif operator == "or":
			variable.value = str(symb1_actual or symb2_actual).lower()
		elif operator == "not":
			variable.value = str(not symb1_actual).lower()

	def get_symb_type_value(self, symb, check_if_initialized=True):

		if symb[0] == "var":
			var = self.get_var(symb, check_if_initialized)
			return var.variable_type, var.value
		elif symb[0] == "int":
			return TYPE_INT, symb[1]
		elif symb[0] == "bool":
			return TYPE_BOOL, symb[1]
		elif symb[0] == "string":
			return TYPE_STRING, symb[1]
		elif symb[0] == "nil":
			return TYPE_NIL, symb[1]

	def get_type(self, var, symb):

		variable = self.get_var(var)
		symb_type, symb_value = self.get_symb_type_value(symb, False)
		variable.variable_type = TYPE_STRING

		if symb_type == TYPE_STRING:
			variable.value = "string"
		elif symb_type == TYPE_INT:
			variable.value = "int"
		elif symb_type == TYPE_BOOL:
			variable.value = "bool"
		elif symb_type == TYPE_NIL:
			variable.value = "nil"
		else:
			variable.value = ""

	def is_equal(self, symb1, symb2):

		symb2_type, symb2_value = self.get_symb_type_value(symb2)
		symb1_type, symb1_value = self.get_symb_type_value(symb1)

		if symb1_type != symb2_type:
			errorMessage(53, "Both symbols in JUMPIFEQ must be same type!\n")

		return symb1_value == symb2_value

	def is_not_equal(self, symb1, symb2):

		symb2_type, symb2_value = self.get_symb_type_value(symb2)
		symb1_type, symb1_value = self.get_symb_type_value(symb1)

		if symb1_type != symb2_type:
			errorMessage(53, "Both symbols in JUMPIFNEQ must be same type!\n")

		return symb1_value != symb2_value

	def exit(self, symb):
		symb_type, symb_value = self.get_symb_type_value(symb)

		if (symb_type == TYPE_INT and 
			symb_value > 0 and 
			symb_value <= 49):
			errorMessage(symb_value, "")
		else:
			errorMessage(57, "invalid exit code")

	def concat_strings(self, var, symb1, symb2):

		variable = self.get_var(var)
		symb1_type, symb1_value = self.get_symb_type_value(symb1)
		symb2_type, symb2_value = self.get_symb_type_value(symb2)

		if symb1_type != TYPE_STRING or symb2_type != TYPE_STRING:
			sys.stderr.write("CONCAT needs two string symbols!\n")
			self.exit_wrong_op(symb1, symb2, types=[TYPE_STRING])

		variable.variable_type = TYPE_STRING
		variable.value = symb1_value + symb2_value

	def len_string(self, var, symb):

		variable = self.get_var(var)
		symb_type, symb_value = self.get_symb_type_value(symb)

		if symb_type != TYPE_STRING:
			sys.stderr.write("STRLEN needs string symbol!\n")
			self.exit_wrong_op(symb, types=[TYPE_STRING])

		variable.variable_type = TYPE_INT
		variable.value = len(symb_value)

	def get_char(self, var, symb1, symb2):

		variable = self.get_var(var)
		symb1_type, symb1_value = self.get_symb_type_value(symb1)
		symb2_type, symb2_value = self.get_symb_type_value(symb2)

		if symb1_type != TYPE_STRING:
			sys.stderr.write("GETCHAR first symbol must be string!\n")
			self.exit_wrong_op(symb1, types=[TYPE_STRING])
		elif symb2_type != TYPE_INT:
			sys.stderr.write("GETCHAR second symbol must be int!\n")
			self.exit_wrong_op(symb2, types=[TYPE_INT])

		variable.variable_type = TYPE_STRING
		try:
			variable.value = symb1_value[symb2_value]
		except IndexError:
			sys.stderr.write("GETCHAR string out of range!\n")
			exit(58)

	def set_char(self, var, symb1, symb2):

		variable = self.get_var(var)
		symb1_type, symb1_value = self.get_symb_type_value(symb1)
		symb2_type, symb2_value = self.get_symb_type_value(symb2)

		if variable.variable_type != TYPE_STRING:
			errorMessage(53, "SETCHAR needs string var!\n")
		elif symb1_type != TYPE_INT:
			sys.stderr.write("SETCHAR first symbol must be int!\n")
			self.exit_wrong_op(symb1, types=[TYPE_INT])
		elif symb2_type != TYPE_STRING:
			sys.stderr.write("SETCHAR second symbol must be string!\n")
			self.exit_wrong_op(symb2, types=[TYPE_STRING])

		try:
			if symb1_value > (len(variable.value) - 1) or symb1_value < 0:
				raise IndexError

			variable.value = variable.value[:symb1_value] + symb2_value[0] + variable.value[symb1_value + 1:]
		except IndexError:
			errorMessage(58, "SETCHAR string out of range!\n")

	def read_var(self, var, var_type):

		var_type = var_type[1]
		try:
			value = file.readline()
		except (EOFError, KeyboardInterrupt):
			value = None

		if var_type == "int":
			var_type = TYPE_INT
			try:
				value = int(value)
			except (ValueError, TypeError):
				value = 0
		elif var_type == "bool":
			var_type = TYPE_BOOL
			if value is not None:
				value = value.lower()
			if value != "true" and value != "false":
				value = "false"
		elif var_type == "string":
			var_type = TYPE_STRING
			if value is None:
				value = ""

		variable = self.get_var(var)
		variable.variable_type = var_type
		variable.value = value

	def int_to_char(self, var, symb):

		variable = self.get_var(var)
		symb_type, symb_value = self.get_symb_type_value(symb)

		if symb_type != TYPE_INT:
			sys.stderr.write("INT2CHAR requires symbol with int type!\n")
			self.exit_wrong_op(symb, types=[TYPE_INT])

		variable.variable_type = TYPE_STRING

		try:
			variable.value = chr(symb_value)
		except ValueError:
			errorMessage(58, "INT2CHAR requires symbol with valid ordinary char value in UNICODE!\n")

	def stri_to_int(self, var, symb1, symb2):

		variable = self.get_var(var)
		symb1_type, symb1_value = self.get_symb_type_value(symb1)
		symb2_type, symb2_value = self.get_symb_type_value(symb2)

		if symb1_type != TYPE_STRING:
			sys.stderr.write("STRI2INT must have first symbol with string type!\n")
			self.exit_wrong_op(symb1, types=[TYPE_STRING])
		elif symb2_type != TYPE_INT:
			sys.stderr.write("STRI2INT must have second symbol with int type!\n")
			self.exit_wrong_op(symb2, types=[TYPE_INT])

		variable.variable_type = TYPE_INT

		try:
			variable.value = ord(symb1_value[symb2_value])
		except IndexError:
			errorMessage(58, "STRI2INT string out of range!\n")

	def exit_wrong_op(self, *symbols, types=None):

		if types is None:
			types = []

		for symb in symbols:
			data_type, value = self.get_symb_type_value(symb)
			if data_type not in types:
				if symb[0] == "var":
					exit(53)
				else:
					exit(52)


class Variable:
	def __init__(self, name):
		self.name = name
		self.variable_type = TYPE_NONE
		self.value = None


class InterpretFactory:

	def __init__(self):

		self.total_inst = 0

		self.labels = []
		self.calls = []
		self.instructions = []
		self.frames = Frames()
		self.variables_factory = VariablesFactory(self.frames)
		self.names_pattern = re.compile("[^A-ZÁ-Ža-zá-ž0-9-\*\$%_&]")

	def addInstruction(self, opcode, args, position):

		if opcode not in INSTRUCTIONS:
			raise parseError("unknown instruction %s" % opcode)
		try:
			if (opcode == "MOVE" or
					opcode == "INT2CHAR" or
					opcode == "STRLEN" or
					opcode == "TYPE" or
					opcode == "NOT"):
				self.checkArgCount(len(args), 2, opcode)
				self.var(args[0])
				self.symb(args[1])
			elif opcode == "READ":
				self.checkArgCount(len(args), 2, opcode)
				self.var(args[0])
				self.type(args[1])
			elif (opcode == "RETURN" or
				  opcode == "CREATEFRAME" or
				  opcode == "PUSHFRAME" or
				  opcode == "POPFRAME" or
				  opcode == "BREAK"):
				self.checkArgCount(len(args), 0, opcode)
			elif (opcode == "ADD" or
				  opcode == "SUB" or
				  opcode == "MUL" or
				  opcode == "IDIV" or
				  opcode == "LT" or
				  opcode == "GT" or
				  opcode == "EQ" or
				  opcode == "AND" or
				  opcode == "OR" or
				  opcode == "STRI2INT" or
				  opcode == "CONCAT" or
				  opcode == "GETCHAR" or
				  opcode == "SETCHAR"):
				self.checkArgCount(len(args), 3, opcode)
				self.var(args[0])
				self.symb(args[1])
				self.symb(args[2])
			elif (opcode == "JUMPIFEQ" or
				  opcode == "JUMPIFNEQ"):
				self.checkArgCount(len(args), 3, opcode)
				self.label(args[0])
				self.symb(args[1])
				self.symb(args[2])
			elif (opcode == "PUSHS" or
				  opcode == "WRITE" or
				  opcode == "DPRINT" or
				  opcode == "EXIT"):
				self.checkArgCount(len(args), 1, opcode)
				self.symb(args[0])
			elif (opcode == "DEFVAR" or
				  opcode == "POPS"):
				self.checkArgCount(len(args), 1, opcode)
				self.var(args[0])
			elif (opcode == "CALL" or
				  opcode == "JUMP"):
				self.checkArgCount(len(args), 1, opcode)
				self.label(args[0])
			elif opcode == "LABEL":
				self.checkArgCount(len(args), 1, opcode)
				self.label(args[0])
				self.add_label(args[0], position)

			self.instructions.append({"opcode": opcode, "args": args})
		except IndexError:
			raise parseError("missing arguments for %s instruction" % opcode)

	def run(self):

		inst_len = len(self.instructions)
		current_inst = 0

		while current_inst < inst_len:

			opCode = self.instructions[current_inst]["opcode"]

			if opCode == "CREATEFRAME":
				self.frames.temporary_frame = []
			elif opCode == "BREAK":
				sys.stderr.write("DEBUG INFO\n")
				sys.stderr.write("Interpreted instruction count: %d\n" % self.total_inst)
				sys.stderr.write("Current instruction number: %d\n" % int(current_inst + 1))
				sys.stderr.write("GF:\nTotal: %d\n" % len(self.frames.global_frame))
				for var in self.frames.global_frame:
					sys.stderr.write("%s (%d): %s\n" % (var.name, var.variable_type, var.value))
				sys.stderr.write("LF:\n")
				if self.frames.local_frame is not None:
					sys.stderr.write("Total: %d\n" % len(self.frames.local_frame))
					for var in self.frames.local_frame:
						sys.stderr.write("%s (%d): %s\n" % (var.name, var.variable_type, var.value))
				else:
					sys.stderr.write("Not initialized!\n")
				if self.frames.temporary_frame is not None:
					sys.stderr.write("TF:\nTotal: %d\n" % len(self.frames.temporary_frame))
					for var in self.frames.temporary_frame:
						sys.stderr.write("%s (%d): %s\n" % (var.name, var.variable_type, var.value))
				else:
					sys.stderr.write("Not initialized!\n")
			elif opCode == "PUSHFRAME":
				self.frames.push_f()
			elif opCode == "POPFRAME":
				self.frames.pop_f()
			elif opCode == "RETURN":
				if len(self.calls) == 0:
					errorMessage(56, "Call stack is empty.")
				current_inst = self.calls.pop()
			elif opCode == "DEFVAR":
				self.variables_factory.def_var(self.instructions[current_inst]["args"][0])
			elif opCode == "JUMP":
				current_inst = self.jump_to_label(self.instructions[current_inst]["args"][0]) - 1
			elif opCode == "EXIT":
				self.variables_factory.exit(self.instructions[current_inst]["args"][0])
			elif opCode == "WRITE":
				self.variables_factory.print_var(self.instructions[current_inst]["args"][0])
			elif opCode == "DPRINT":
				self.variables_factory.print_var(self.instructions[current_inst]["args"][0], True)
			elif opCode == "CALL":
				self.calls.append(current_inst)
				current_inst = self.jump_to_label(self.instructions[current_inst]["args"][0]) - 1
			elif opCode == "READ":
				self.variables_factory.read_var(self.instructions[current_inst]["args"][0],
												self.instructions[current_inst]["args"][1])
			elif opCode == "STRLEN":
				self.variables_factory.len_string(self.instructions[current_inst]["args"][0],
												self.instructions[current_inst]["args"][1])
			elif opCode == "TYPE":
				self.variables_factory.get_type(self.instructions[current_inst]["args"][0],
												self.instructions[current_inst]["args"][1])
			elif opCode == "MOVE":
				self.variables_factory.move_to_var(self.instructions[current_inst]["args"][0],
												self.instructions[current_inst]["args"][1])
			elif opCode == "INT2CHAR":
				self.variables_factory.int_to_char(self.instructions[current_inst]["args"][0],
												self.instructions[current_inst]["args"][1])
			elif (opCode == "ADD" or
				  opCode == "SUB" or
				  opCode == "MUL" or
				  opCode == "IDIV"):
				self.variables_factory.aritmetic_op(self.instructions[current_inst]["args"][0],
													self.instructions[current_inst]["args"][1],
													self.instructions[current_inst]["args"][2],
													opCode.lower())  # op code to lower or check next fnc
			elif (opCode == "LT" or
				  opCode == "GT" or
				  opCode == "EQ"):
				self.variables_factory.relation_op(self.instructions[current_inst]["args"][0],
													self.instructions[current_inst]["args"][1],
													self.instructions[current_inst]["args"][2],
													opCode.lower())
			elif (opCode == "AND" or
				  opCode == "OR"):
				self.variables_factory.bool_op(self.instructions[current_inst]["args"][0],
												self.instructions[current_inst]["args"][1],
												self.instructions[current_inst]["args"][2],
												opCode.lower())
			elif opCode == "NOT":
				self.variables_factory.bool_op(self.instructions[current_inst]["args"][0],
												self.instructions[current_inst]["args"][1],
												self.instructions[current_inst]["args"][1],
												opCode.lower())
			elif opCode == "STRI2INT":
				self.variables_factory.stri_to_int(self.instructions[current_inst]["args"][0],
													self.instructions[current_inst]["args"][1],
													self.instructions[current_inst]["args"][2])
			elif opCode == "JUMPIFEQ":
				if (self.variables_factory.is_equal(self.instructions[current_inst]["args"][1],
													self.instructions[current_inst]["args"][2])):
					current_inst = self.jump_to_label(self.instructions[current_inst]["args"][0]) - 1
			elif opCode == "JUMPIFNEQ":
				if (self.variables_factory.is_not_equal(self.instructions[current_inst]["args"][1],
														self.instructions[current_inst]["args"][2])):
					current_inst = self.jump_to_label(self.instructions[current_inst]["args"][0]) - 1
			elif opCode == "CONCAT":
				self.variables_factory.concat_strings(self.instructions[current_inst]["args"][0],
													self.instructions[current_inst]["args"][1],
													self.instructions[current_inst]["args"][2])
			elif opCode == "GETCHAR":
				self.variables_factory.get_char(self.instructions[current_inst]["args"][0],
												self.instructions[current_inst]["args"][1],
												self.instructions[current_inst]["args"][2])
			elif opCode == "SETCHAR":
				self.variables_factory.set_char(self.instructions[current_inst]["args"][0],
												self.instructions[current_inst]["args"][1],
												self.instructions[current_inst]["args"][2])

			current_inst += 1
			self.total_inst += 1

		self.stat_vars = self.frames.stat_vars

	@staticmethod
	def checkArgCount(given, req, opCode):
		if given != req:
			raise ET.ParseError("Incorect number of arguments for %s instruction." % opCode)

	def var(self, arg):
		if arg[0] != "var":
			raise parseError("expected variable")
		try:
			frame, name = arg[1].split("@")
		except ValueError:
			raise parseError("variable has wrong format")

		if self.names_pattern.match(name) or name[0].isdigit():
			raise parseError("variable name has wrong format")

		if frame not in FRAMES:
			raise parseError("unknown frame")

	def symb(self, arg):
		if arg[0] == "var":
			self.var(arg)
		elif arg[0] == "nil":
			if arg[1] != "nil":
				raise parseError("invalid nil")
		elif arg[0] == "bool":
			if arg[1] != "true" and arg[1] != "false":
				raise parseError("unknown bool value")
		elif arg[0] == "int":
			try:
				arg[1] = int(arg[1])
			except (ValueError, TypeError):
				raise parseError("wrong int literal")
		elif arg[0] == "string":

			if arg[1] is None:
				arg[1] = ""
			if " " in arg[1]:
				raise parseError("found whitespace in string literal")
			elif "#" in arg[1]:
				raise parseError("found # char in string literal")

			arg[1] = unescape(arg[1])
			i = 0

			for char in arg[1]:
				if char == "\\":
					try:
						xyz = int(arg[1][i+1] + arg[1][i+2] + arg[1][i+3])
						arg[1] = arg[1][:i] + chr(xyz) + arg[1][i+4:]
						i -= 3
					except (ValueError, IndexError):
						raise parseError("wrong string escape sequence")
				i += 1
		else:
			raise ET.ParseError("symbol can only be var, int, bool, nil or string")

	def label(self, arg):
		if arg[0] != "label":
			raise parseError("expected label")
		if self.names_pattern.match(arg[1]) or arg[1][0].isdigit():
			raise parseError("label name has wrong format")

	@staticmethod
	def type(arg):
		if arg[0] != "type":
			raise parseError("expected type")
		if arg[1] not in TYPES:
			raise parseError("unexpected type")

	def find_label(self, name):
		for label in self.labels:
			if label[0] == name:
				return label
		return None

	def add_label(self, arg, position):
		if not self.find_label(arg[1]):
			self.labels.append([arg[1], position])
		else:
			sys.stderr.write("Label %s already exists!\n" % arg[1])
			exit(52)

	def jump_to_label(self, arg):
		self.total_inst += 1
		label = self.find_label(arg[1])

		if not label:
			sys.stderr.write("Label %s not found!\n" % arg[1])
			exit(52)
		else:
			return label[1] - 1


file = sys.stdin

def main():


	parser = argparse.ArgumentParser()
	parser.add_argument("--source")
	parser.add_argument("--input")

	args = parser.parse_args()

	if not args.source and not args.input:
	    sys.stderr.write("Missing --source or --input parametr!\n")
	    exit(10)

	if args.input:
		file = open(args.input,"r")

	try:
		XMLtree = ET.parse(args.source)

		root = XMLtree.getroot()
		if root.tag != "program":
			errorMessage(32, "Wrong root element.")

		if "language" not in root.attrib:
			errorMessage(32, "No language attribute.")

		for attrib, value in root.attrib.items():
			if attrib == "language":
				if value != "IPPcode19":
					errorMessage(32, "Invalid language attribute.")
			elif attrib != "name" and attrib != "describtion":
				errorMessage(32, "Invalid program element.")

		interpret = InterpretFactory()
		order = 1

		for child in root:
			opcode = None
			if child.tag != "instruction":
				errorMessage(32, "program element can contain only instruction subelements")

			if "opcode" not in child.attrib or "order" not in child.attrib:
				errorMessage(32, "missing attribute opcode or order in program element")

			for attrib, value in child.attrib.items():
				if attrib == "opcode":
					opcode = value
				elif attrib == "order":
					if int(value) != order:
						errorMessage(32, "bad instruction order")
					order += 1
				else:
					errorMessage(32, "instruction element can only contain opcode or order argument")

			args_list = []
			for arg in child:
				try:
					arg_num = int(arg.tag[3:])
				except ValueError:
					errorMessage(32, "wrong argument number")

				if "type" not in arg.attrib:
					errorMessage(32, "missing type attribute in arg element")

				if len(arg.attrib) != 1:
					errorMessage(32, "Not allowed attributes in arg element")

				types = ["int", "nil", "bool", "string", "label", "type", "var", "label"]

				if arg.attrib["type"] not in types:
					errorMessage(32, "Not allowed arg type")
				while arg_num > len(args_list):
					args_list.append(None)

				if args_list[arg_num - 1] is not None:
					errorMessage(32, "argument already set")

				args_list[arg_num - 1] = [arg.attrib["type"], arg.text]

			if None in args_list:
				errorMessage(32, "missing arguments")

			interpret.addInstruction(opcode, args_list, order)
		interpret.run()

	except FileNotFoundError:
		errorMessage(11, "Can not open file!")
	except ET.ParseError as error:
		errorMessage(32, str(error))
	except parseError as error:
		errorMessage(32, str(error))
	sys.exit(0)


if __name__ == '__main__':
	main()